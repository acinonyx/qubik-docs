###################
Command and Control
###################

Intro
=====
QUBIK pocketqube twins are radio amateur service satellites that can be commanded by radio amateurs around the world.
The purpose of documenting this functionality is to enable more radio amateurs around the world to gain hands-on
experience with C&C of an actual satellite, testing their setups and having a rewarding experience.

.. warning::
    Licensed radio amateurs only.

    Before attempting to transmit any signals to QUBIK satellites please ensure that you are compliant with any
    local laws and regulations.
    If uncertain, reach out to your national radio amateur association.

Hardware setup
==============
A typical command and control station for QUBIK satellites would include the following hardware components:

**Components:**

+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+
| Part        | Source-1                                                                                            | Source-2                            |
+=============+=====================================================================================================+=====================================+
| PA          |                                                                                                     |                                     |
+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+
| RF Switch   | https://gitlab.com/rf-modules/rf-switch                                                             |                                     |
+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+
| LNA         | https://gitlab.com/rf-modules/rf-low-noise-amplifier                                                | https://www.wimo.com/en/lna-5000    |
+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+
| Bias T      | https://gitlab.com/rf-modules/bias-t                                                                | https://www.wimo.com/en/dcc-5000pro |
+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+
| Client      | Follow instructions on the same page                                                                |                                     |
+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+
| Antenna     | https://www.wimo.com/en/x-quad                                                                      |                                     |
+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+
| Cabling     |                                                                                                     |                                     |
+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+
| Rotator     | https://wiki.satnogs.org/Rotators                                                                   |                                     |
+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+
| SDR with Tx | PlutoSDR                                                                                            | USRP                                |
+-------------+-----------------------------------------------------------------------------------------------------+-------------------------------------+

**NOTE**: The RF-switch must switch from Rx to Tx and Tx to Rx at most
in 10 ms.

.. list-table::

    * - .. figure:: img/1-ant.png

           one antenna setup

      - .. figure:: img/2-ant.png

           two antennas setup

.. figure:: img/qubik_tx_1.jpg
   :width: 800px
   :alt: QUBIK Tx setup
   :align: center

   Indicative QUBIK Tx setup.

Two Antennas Set-Up
-------------------

This set-up uses the same components but with additional antenna for Tx.
The problem with this set-up is the power that is received while the
other antenna is transmitting.

A experiment with 2 x-quad antennas are done. The antennas are placed at
distance of 1.7 m. The PA transmits 37 dBm before a cable of 5m and the
antenna. The Rx antenna receives after a 2 m cable -8 dBm. The experiment
took place in open air field and the measurement tool is a calibrated
SA.

**The results of experiment may not repeatable with other antennas.**

The LNA can handle in the input the received power. The
`SDR <https://kb.ettus.com/B200/B210/B200mini/B205mini#Input_Power_Levels>`__
either can handle the receiving power or the LNA gain when is OFF it is
enough to protect the SDR.

Power Amplifier
---------------

.. figure:: img/pa-assembly.png
   :alt: pa-assembly

   PA Assembly

+-----------------------------------+-----------------------------------+
| Part                              | Qty                               |
+===================================+===================================+
| `Mitsubishi RA07H4047M RF Power   | 1                                 |
| Amplifier                         |                                   |
| Module <https://e                 |                                   |
| nigma-shop.com/index.php?option=c |                                   |
| om_hikashop&ctrl=product&task=sho |                                   |
| w&name=mitsubishi-ra07h4047m-rf-p |                                   |
| ower-amplifier-module&cid=753>`__ |                                   |
+-----------------------------------+-----------------------------------+
| SMA Female, Chassis Panel Mount   | 1                                 |
+-----------------------------------+-----------------------------------+
| N-Type Female, Chassis Panel      | 1                                 |
| Mount                             |                                   |
+-----------------------------------+-----------------------------------+
| SMA Female, Chassis Panel Mount   | 1                                 |
+-----------------------------------+-----------------------------------+
| RG-58 C/U MIL C17-F Coaxial Cable | ~10cm                             |
+-----------------------------------+-----------------------------------+
| `RG-316/U <https://gr.mouser.com  | ~12cm                             |
| /ProductDetail/Belden-Wire-Cable/ |                                   |
| 83284-009100?qs=sGAEpiMZZMuwsoYAV |                                   |
| Cu3CBEYHTiwy9VRigdwGlPzcCM%3D>`__ |                                   |
+-----------------------------------+-----------------------------------+
| PG9 Cable Gland                   | 1                                 |
+-----------------------------------+-----------------------------------+
| Stranded Cable ~1.5mm^2 (16AWG)   | 1m                                |
| Black, Red, Yellow                |                                   |
+-----------------------------------+-----------------------------------+
| Screw M2.5 L6 DIN7985 INOX        | 4                                 |
+-----------------------------------+-----------------------------------+
| Die-cast aluminum enclosure,      | 1                                 |
| 125x80x57 IP66 (or similar)       |                                   |
+-----------------------------------+-----------------------------------+

RA07H4047M Performance
----------------------

+-----------+-----------+-----------+-----------+-----------+-----------+
| Pin(mW)   | Pout(W)   | VDD (V)   | VGG (V)   | Pdc(W) at | Pdc(W) at |
|           |           |           |           | Tx        | idle      |
+===========+===========+===========+===========+===========+===========+
| 12.6      | 6         | 12        | 3         | 12        | 3.3       |
+-----------+-----------+-----------+-----------+-----------+-----------+

RA07H4047M Modification
-----------------------

The RA07H4047M board needs to be modified to remove the input attenuator
and work with the RF out of a USRP B200 directly.

The modification is the removal of R1 through R4 and replacement of R5
and R6 with 0Ohm resistors.

USRP B200mini - Power Calibration
---------------------------------

The PA is drive by the `USRP
B200mini <https://www.ettus.com/all-products/usrp-b200mini/>`__. Power
Calibration of USRP by using
`SoapyUHD <https://github.com/pothosware/SoapyUHD/wiki>`__.

=========================== ============
Setting [0, 89.75, 0.25] dB Output (dBm)
=========================== ============
12                          -54
20                          -45.8
32                          -34
47                          -19
63                          -2.85
70                          4.21
73                          7
**76**                      **10**
**77**                      **11**
**78**                      12
79                          **13**
80                          14
81                          14.81
82                          15.54
83                          16.16
84                          16.69
85                          17.03
86                          17.27
=========================== ============

**The absolute maximum input of RA07H4047M is 30 mW or 14 dBm.**

Thermal Testing
---------------

Two temperatures sensors are placed in the enclosure. One sensor (T1) is
placed at the bottom of the box near the PA module and the second (T2)
at the lid of enclose.

The temperature at idle state of PA is stabilized at ~31.3°C after 50 minutes.

+-------+-------+---------+-------------------------------------------+
| T1 (  | T2 (  | Time    | State                                     |
| °C )  | °C )  | (min)   |                                           |
+=======+=======+=========+===========================================+
| 20.5  | 20.5  | 0       | OFF                                       |
+-------+-------+---------+-------------------------------------------+
| 26.4  | 24.8  | 10      | ON, Input terminated with 50Ohm, PDC =    |
|       |       |         | 3.324W @ 12V                              |
+-------+-------+---------+-------------------------------------------+
| 28.5  | 26.9  | 20      |                                           |
+-------+-------+---------+-------------------------------------------+
| 30    | 28.5  | 30      |                                           |
+-------+-------+---------+-------------------------------------------+
| 30.8  | 29.3  | 40      |                                           |
+-------+-------+---------+-------------------------------------------+
| 31.3  | 29.9  | 50      |                                           |
+-------+-------+---------+-------------------------------------------+

Then a CW signal is applied to PA module.

+-------+-------+-----------+-----------------------------------------+
| T1 (  | T2 (  | Duration  | State                                   |
| °C )  | °C )  | (min)     |                                         |
+=======+=======+===========+=========================================+
| 35.1  | 31.7  | 3         | ON, Input +12 dBm, output 6W PDC = 12W @|
|       |       |           | 12V                                     |
+-------+-------+-----------+-----------------------------------------+
| 33.5  | 32    | 10        | ON, Input terminated with 50Ohm, PDC =  |
|       |       |           | 3.324W @ 12V                            |
+-------+-------+-----------+-----------------------------------------+
| 36.6  | 33.3  | 5         | ON, Input +12 dBm, output 6W PDC = 12W @|
|       |       |           | 12V                                     |
+-------+-------+-----------+-----------------------------------------+
| 37.8  | 34.3  | 2.5       | ON, Input +12 dBm, output 6W PDC = 12W @|
|       |       |           | 12V                                     |
+-------+-------+-----------+-----------------------------------------+
| 38.3  | 34.9  | 2.5       | ON, Input +12 dBm, output 6W PDC = 12W @|
|       |       |           | 12V                                     |
+-------+-------+-----------+-----------------------------------------+
| 35.7  | 33.6  | 10        | ON, Input terminated with 50Ohm, PDC =  |
|       |       |           | 3.324W @ 12V                            |
+-------+-------+-----------+-----------------------------------------+

It seems that the PA module operates faraway from 90°C (maximum operating
temperature). Also the temperature of PA module at idle is approximately
+10°C above the environment temperature (20.5°C).

.. figure:: img/hpa_flir.png
   :alt: Thermal Camera Result
   :align: center

   Thermal Camera Result.

Software setup
==============
In order to command and control a QUBIK satellite you will need a specific software stack to handle all the tasks necessary.

1. `GNU Radio 3.8.2 <https://www.gnuradio.org/>`_
2. `gr-soapy (and appropriate SDR specific drivers) <https://gitlab.com/librespacefoundation/gr-soapy>`_
3. `gr-satnogs <https://gitlab.com/librespacefoundation/satnogs/gr-satnogs>`_
4. `Gpredict (for Doppler and rotator control) <http://gpredict.oz9aec.net/>`_
5. `QUBIK transceiver flow graph <https://gitlab.com/librespacefoundation/qubik/qubik-comms-sw/-/blob/master/test/flowgraphs/qubik_transceiver.grc>`_ (The GNU Radio transceiver)
6. `osdlp-operator <https://gitlab.com/librespacefoundation/osdlp-operator>`_ (The command and control program)
7. `qubik_listener <https://gitlab.com/librespacefoundation/qubik/qubik_listener>`_ (The packet forwarder to SatNOGS DB)


.. figure:: img/qubik_tx_sw.png
   :width: 800px
   :alt: QUBIK Tx Software
   :align: center

   QUBIK Command Software diagram

.. tip::
   Your command software stack can be split in two different setups.

   For example, a Rpi4 computer near the antenna with the SDR attached to it controlling also the rotator, while a second host over the network can handle the command and control software (osdlp-operator) and the Gpredict (for Doppler and rotator pointing calculations) for a pass. Of course those can be hosted under a single computer if cabling and specifics allow.

.. tip::
   Setup your Gpredict Radio Interface with Radio Type: Full-Duplex and leave anything else as default.

Running
=======

When all hardware and software are ready and installed, just before AOS the following should happen:

#. Run :code:`rigctld -T 127.0.0.1 -m 1 -v` to create a rigctl server to wait for incoming clients (transceiver and Gpredict)
#. Run the appropriate :code:`rotctld` command to wait for incoming Gpredict connection
#. Run the :code:`qubik_transceiver.py` with the appropriate arguments

   * For PlutoSDR :code:`./qubik_transceiver.py --soapy-rx-device 'driver=plutosdr' --antenna-rx 'A_BALANCED' --antenna-tx 'A_BALANCED'`

   * For USRP use: :code:`./qubik_transceiver.py --soapy-rx-device 'driver=uhd' --antenna-rx 'RX2' --antenna-tx 'Tx/Rx'`

#. Run the osdlp-operator with the fop configuration: :code:`osdlp-operator fop_configuration.cfg`
#. Run :code:`netcat -lu -p 16880` to see all debugging messages from osdlp-operator
#. Run :code:`gpredict` and start the pass operations (Antenna and Radio control) for your selected QUBIK
#. Run :code:`qubik_listener.py` to forward any received frames to SatNOGS DB (and see them locally)

   * Example for station :code:`./qubik_listener.py --callsign SV1QVE --lon 36.970945N --lat 36.970945E`

#. Return to osdlp-operator and command QUBIK.

.. note::
   If your running session is a testing one (with QUBIK EMs) you *need* to disable the rigctl message block from the qubik_transceiver flow graph, for your session to run. No need to Doppler compensate on ground.
